Via https://github.com/ushahidi/tenfour/issues/112

# Issue Name
Check-ins and messages should have "severity" levels #112

# Please describe the problem.
When a typhoon has occurred often different areas have different levels of severity and danger that are useful to know for the relief and aid organizations/NGO's managing the response to the Typhoon.
The relief and aid organizations/NGO's need to know how 'severe' a person or a location situation is. In this case (Taiwan Typhoons) a person will be describing severity of possibly not only themselves (potential injuries or loss of life) but also the severity of damage to agriculture or land.

Being able to differentiate between person and place severity is important.

Also, owners of land (farmers) volunteers responding to a crisis and NGO's may regard 'damage' differently and therefore an understanding of the severity of a person's needs is critical.

Also to consider, landowners/farmers are often required to record the damage (photo, video, writing etc.) in order to receive an optimum subsidy from the government in recovering their land/business but the government can move slowly and some kinds of recovery are immediate (that which is dangerous to others)

Research insight: Farmers send photos to show damage and ask for help on platforms like Facebook.

# Who are we designing for?
We are designing for at least two user groups both during & after disaster

1 -NGO Leads or people managing a TenFour organization which includes farmers as responders to check-ins or as managers of the TenFour organization. Otherwise referred to as an 'admin'. Typically have the role types of Owner and Admin in TenFour. These users program, manage and send out the check-ins. These are the people that want to know the severity level of a situation/farm.

2 - The volunteers or TenFour check-in responders (could be farm owners) that are responding to, or signaling to NGO's/TenFour organizations. These people are likely to be sending the severity level and also backing-up another persons sent severity level.

# Describe what the user needs to do
Be able to send a check-in requesting a severity level for a particular location and/or person or
receive a severity location message via a person/location.

Have that severity level information backed up by an independent source and signal to the location/person that raised the severity level what is happening.

# Describe alternatives you've considered
Currently, people responding to check-ins can send messages with their check-in responses but this information is not easily scannable.

# What is success for our user
Admin/TenFour organzation owners are able to quickly see severity levels from different responders and locations with detailed info available

Have severity levels backed-up in certain cases.

Have a way of checking severity levels as locations in order to plan effectively.

# What are our design constraints?
Requires:
Mobile telecom connection or internet connection.
Uses/already logged onto a TenFour org


# Examples of designer responses to this challenge

Our team had done the homework and the Adobe XD links are as follows:
User-Victim : post rescue request
https://xd.adobe.com/spec/250564f4-f6ba-4166-77a7-9350de26c05c-5334/
User-Admin : arrange volunteers and define region severity levels
https://xd.adobe.com/view/ae5fb84d-f44f-4f02-7c48-c90b27a8a8eb-d86e/

User-Admin_final: arrange volunteers and define region severity levels
https://xd.adobe.com/view/6424f0d5-93a7-441e-477d-7c55cf3c379e-1215/

Online-discussion on google document:
We defined severity level first, then we discussed what each type of role can do or what kind of actions they can take

https://docs.google.com/document/d/1f7JZ0N2LUZZNhJuiLfe_msT4PbkuizsXsqZfgH_SWnI/edit
https://drive.google.com/file/d/16eKaCMZcID3fmqGIWcLOEtc5VNinxBLv/view?usp=sharing
On the TenFour system as a ‘person’.